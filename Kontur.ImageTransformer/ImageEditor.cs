using SixLabors.ImageSharp;
using SixLabors.Primitives;
using System.Net;

namespace Kontur.ImageTransformer
{
    class ImageEditor
    {
        public HttpListenerContext Context { get; set; }
        public Filter Filter { get; set; }

        public ImageEditor(HttpListenerContext context)
        {
            Context = context;
        }

        public void SendOutputFromContext()
        {
            Filter = new Filter();

            if (!Filter.CheckCoordinates(Context))
            {
                Context.Response.StatusCode = 400;
                Context.Response.OutputStream.Close();
                return;
            }

            using (Image<Rgba32> image = Image.Load<Rgba32>(Context.Request.InputStream))
            {
                var coordinatesNormalized = Filter.NormalizeCoordinates(image.Width, image.Height);

                if (!coordinatesNormalized)
                {
                    Context.Response.StatusCode = 204;
                    Context.Response.OutputStream.Close();
                    return;
                }

                var rectangle = new Rectangle(Filter.XCoordinate, Filter.YCoordinate, Filter.Width, Filter.Height);

                image.Mutate(imag => imag.Crop(rectangle));

                switch (Filter.Type)
                {
                    case Filter.FilterType.Grayscale:
                        {
                            image.Mutate(imag => imag
                                .Grayscale());
                            break;
                        }

                    case Filter.FilterType.Sepia:
                        {
                            image.Mutate(imag => imag
                                .Sepia());
                            break;
                        }

                    case Filter.FilterType.Threshold:
                        {
                            image.Mutate(imag => imag
                                .BinaryThreshold((float)(Filter.ThresholdValue) / 100));
                            break;
                        }
                }

                image.SaveAsPng(Context.Response.OutputStream);
                Context.Response.OutputStream.Close();
            }
        }
    }
}