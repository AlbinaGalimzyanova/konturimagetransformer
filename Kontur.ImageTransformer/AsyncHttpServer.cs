using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Kontur.ImageTransformer
{
    class AsyncHttpServer : IDisposable
    {
        private readonly HttpListener listener;
        private Thread listenerThread;
        private bool disposed;
        private volatile bool isRunning;
        private Stopwatch stopwatch = new Stopwatch();
        private int iteration;
        private Queue<HttpListenerContext> queue = new Queue<HttpListenerContext>();
        private long elapsed;
        private long previousElapsed = 1000;
        private const int iterationCheck = 500;
        private const int millisecondsThreshold = 900;

        public AsyncHttpServer()
        {
            listener = new HttpListener();
        }

        public void Start(string prefix)
        {
            lock (listener)
            {
                if (!isRunning)
                {
                    listener.Prefixes.Clear();
                    listener.Prefixes.Add(prefix);
                    listener.Start();

                    listenerThread = new Thread(Listen)
                    {
                        IsBackground = true,
                        Priority = ThreadPriority.Highest
                    };
                    listenerThread.Start();

                    isRunning = true;
                }
            }
        }

        public void Stop()
        {
            lock (listener)
            {
                if (!isRunning)
                    return;

                listener.Stop();

                listenerThread.Abort();
                listenerThread.Join();

                isRunning = false;
            }
        }

        public void Dispose()
        {
            if (disposed)
                return;

            disposed = true;

            Stop();

            listener.Close();
        }

        private void Listen()
        {
            while (true)
            {
                if (listener.IsListening)
                {
                    var context = listener.GetContext();
                    queue.Enqueue(context);

                    if (elapsed < millisecondsThreshold)
                        Task.Run(() => HandleContextAsync(queue.Dequeue()));
                    else
                        {
                            context.Response.StatusCode = 429;
                            context.Response.OutputStream.Close();
                        }
                }
                else
                    Thread.Sleep(0);
            }
        }

        private async Task HandleContextAsync(HttpListenerContext context)
        {
            if (iteration % iterationCheck == 0)
                stopwatch.Start();

            iteration++;

            var imageEditor = new ImageEditor(context);
            imageEditor.SendOutputFromContext();

            if (iteration % iterationCheck == 0)
            {
                stopwatch.Stop();
                elapsed = stopwatch.Elapsed.Milliseconds;
                stopwatch.Reset();

                if (elapsed > previousElapsed)
                {
                    var tmp = iterationCheck * millisecondsThreshold / (elapsed - previousElapsed);

                    if (tmp <= iterationCheck)
                    {
                        foreach (var element in queue)
                        {
                            element.Response.StatusCode = 429;
                            element.Response.OutputStream.Close();
                        }

                        queue.Clear();
                        elapsed = 0;
                    }
                }

                previousElapsed = elapsed;
            }
        }
    }
}