using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace Kontur.ImageTransformer
{
    class Filter
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public FilterType Type { get; set; }
        public int? ThresholdValue { get; set; }

        public enum FilterType
        {
            Grayscale,
            Sepia,
            Threshold,
        }

        public bool CheckCoordinates(HttpListenerContext context)
        {
            var urlParameters = context.Request.RawUrl.Split('/');

            if (urlParameters.Count() != 4)
                return false;

            var coordinates = urlParameters[3].Split(',');

            if (coordinates.Count() != 4)
                return false;

            var x = XCoordinate;
            var y = YCoordinate;
            var width = Width;
            var height = Height;

            var xParsed = Int32.TryParse(coordinates[0], out x);
            var yParsed = Int32.TryParse(coordinates[1], out y);
            var widthParsed = Int32.TryParse(coordinates[2], out width);
            var heightParsed = Int32.TryParse(coordinates[3], out height);

            if (!(xParsed || yParsed || widthParsed || heightParsed))
                return false;

            var filterString = urlParameters[2].ToLower();

            if (filterString == "grayscale")
                Type = FilterType.Grayscale;
            else if (filterString == "sepia")
                Type = FilterType.Sepia;
            else if (filterString.StartsWith("threshold"))
            {
                var regex = new Regex(@"^threshold\(\d\d*\)$");

                if (regex.IsMatch(filterString))
                {
                    Type = FilterType.Threshold;
                    var numberParsed = filterString.Substring(10, filterString.Length - 11);
                    ThresholdValue = Int32.Parse(numberParsed);
                }
                else
                    return false;
            }
            else
                return false;

            return true;
        }

        public bool NormalizeCoordinates(int width, int height)
        {
            if (Width < 0)
            {
                XCoordinate += Width;
                Width *= -1;
            }

            if (Height < 0)
            {
                YCoordinate += Height;
                Height *= -1;
            }

            if (XCoordinate < 0)
            {
                Width += XCoordinate;
                XCoordinate = 0;
            }

            if (YCoordinate < 0)
            {
                Height += YCoordinate;
                YCoordinate = 0;
            }

            if (XCoordinate + Width > width)
                Width = width - XCoordinate;

            if (YCoordinate + Height > height)
                Height = height - YCoordinate;

            if (Width <= 0 || Height <= 0)
            {
                return false;
            }

            return true;
        }
    }
}